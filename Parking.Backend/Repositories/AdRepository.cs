﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Integration;
using Models;

namespace Repositories
{
    public class AdRepository:IAdRepository
    {
        private readonly IAdGateway _adGateway;
        private readonly SqlConnection _connection;
        public AdRepository(SqlConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            _connection = connection;
        }

        //public async Task<Ad> GetAd()
        //{
        //    Boolean timeout = false;
        //    Ad returnAd = null;
        //    try
        //    {
        //        returnAd = _adGateway.GetFreshAd();
        //    }
        //    catch (Exception e) //TODO fix exception
        //    //catch (HttpRequestException e)
        //    {
        //        timeout = true;
        //        returnAd = await FetchCachedAd();
        //    }
        //    if (returnAd.ImageData.Contains("UPS!!"))
        //    {
        //        returnAd = await FetchCachedAd();
        //    }
        //    else if(!timeout)
        //    {
        //        returnAd.Timestamp = DateTime.Now;
        //       await CachAd(returnAd);
        //    }
        //    return returnAd;
        //}

        public async Task<Ad> FetchCachedAd()
        {
            Ad cachedAd = null;
            var sqlFetchAd = "SELECT TOP 1 * " +
                                "FROM Advertisement " +
                                "ORDER BY Timestamp DESC";
            cachedAd = (await _connection.QueryAsync<Ad>(sqlFetchAd)).SingleOrDefault();
            return cachedAd;
        }

        public async Task CachAd(Ad adToBeCached)
        {
            var sqlInsert = "INSERT INTO Advertisement " +
                            "VALUES " +
                            "(@ImageData, @Timestamp)";
            await _connection.ExecuteAsync(sqlInsert, new
            {
                ImageData = adToBeCached.ImageData,
                Timestamp = adToBeCached.Timestamp
            });
        }
    }
}
