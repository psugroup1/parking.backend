﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Repositories
{
    public interface IAdRepository
    {
        //Task<Ad> GetAd();
        Task<Ad> FetchCachedAd();
        Task CachAd(Ad adToBeCached);
    }
}
