﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using API.Controllers;
using Autofac;
using Autofac.Integration.WebApi;
using Integration;
using Repositories;

namespace API.App_Start
{
    public class Bootstrapper
    {
        public static ILifetimeScope InitializeContainer()
        {
            var config = GlobalConfiguration.Configuration;

            var builder = new ContainerBuilder();
            RegisterComponents(builder);
            ILifetimeScope container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            return container;
        }

        private static void RegisterComponents(ContainerBuilder builder)
        {
            builder.RegisterType<AdController>().As<IAdController>();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<AdRepository>().As<IAdRepository>();
            builder.RegisterType<AdGateway>().As<IAdGateway>();
            builder.Register(c =>
            {
                var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LibraryGroup1ConnectionString"].ConnectionString);
                connection.Open();
                return connection;
            }).As<SqlConnection>().SingleInstance();
        }
    }
}