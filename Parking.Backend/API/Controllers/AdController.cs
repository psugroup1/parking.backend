﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Integration;
using Models;
using Repositories;

namespace API.Controllers
{
    [RoutePrefix("Ad")]
    public class AdController: ApiController, IAdController
    {
        private readonly IAdRepository _adRepository;
        private readonly IAdGateway _adGateway;
        public AdController(IAdRepository adRepository, IAdGateway adGateway)
        {
            _adRepository = adRepository;
            _adGateway = adGateway;
        }

        [HttpGet, Route("GetAd")]
        public async Task<IHttpActionResult> GetAd()
        {
            //Boolean timeout = false;
            Ad returnAd = new Ad();
            try
            {
                returnAd = _adGateway.GetFreshAd();
            }
            catch (Exception e) //TODO fix exception
            {
                //Fik timeout
                //timeout = true;
                returnAd.ImageData = "";
            }

            if (returnAd.ImageData.Contains("UPS!!") || returnAd.ImageData == "")
            {
                //Hvis der er timeout, eller fejlbesked
                returnAd = await _adRepository.FetchCachedAd();
            }
            else
            {
                //Ingen problemer. sætter timestamp og cacher ad
                returnAd.Timestamp = DateTime.Now;
                await _adRepository.CachAd(returnAd);
            }

            if (returnAd == null)
            {
                return NotFound();
            }
            return Ok(returnAd);
        }



        //public async Task<Ad> GetAd()
        //{
        //    Boolean timeout = false;
        //    Ad returnAd = null;
        //    try
        //    {
        //        returnAd = _adGateway.GetFreshAd();
        //    }
        //    catch (Exception e) //TODO fix exception
        //    //catch (HttpRequestException e)
        //    {
        //        timeout = true;
        //        returnAd = await FetchCachedAd();
        //    }
        //    if (returnAd.ImageData.Contains("UPS!!"))
        //    {
        //        returnAd = await FetchCachedAd();
        //    }
        //    else if (!timeout)
        //    {
        //        returnAd.Timestamp = DateTime.Now;
        //        await CachAd(returnAd);
        //    }
        //    return returnAd;
        //}
    }
}