﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    public interface IAdController
    {
        Task<IHttpActionResult> GetAd();
    }
}
