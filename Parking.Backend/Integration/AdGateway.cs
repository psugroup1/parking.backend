﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Integration
{
    public class AdGateway : IAdGateway
    {
        public Ad GetFreshAd()
        {
            var freshAd = new Ad();
            var client = GetHttpClient(1);
            var response = client.GetAsync($"http://dm.sof60.dk:81/api/ad");
            response.Result.EnsureSuccessStatusCode(); //Smider exception hvis vi timeouter
            freshAd = response.Result.Content.ReadAsAsync<Ad>().Result;
            return freshAd;
        }

        private HttpClient GetHttpClient(double timeOutSeconds)
        {
            var client = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true });
            //client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseAdress"]);
            //client.BaseAddress = new Uri("http://localhost:1659/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue($"application/json"));
            client.Timeout = TimeSpan.FromSeconds(timeOutSeconds);
            return client;
        }
    }
}
