﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Ad
    {
        public string ImageData { get; set; }
        public DateTime? Timestamp { get; set; }

    }
}
